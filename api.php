<?php
session_start();
include_once(__DIR__.'/database/Connection.php');
include_once(__DIR__.'/includes/class/Equipo.php');
$_conection = new Connection();

$action = "";
$team = "";
$newName = "";
$status = 'success';
$message = 'Success';

if(isset($_POST['action'])){
    $action= $_POST['action'];
}

if(isset($_POST['team'])){
    $team= $_POST['team'];
}

if(isset($_POST['newName'])){
    $newName= $_POST['newName'];
}


$name = $team;
$players = array();
$_team = new Equipo($team);
if($_team->existeEquipo()){
    $name = $_team->getNombre();
    $players = $_team->getJugadores();

    if(count($players)==0){
        $status = 'error';
        $message = 'No hay payers en '.$name;

    }

    if($action='updateTeam'){
        $_teamNew = new Equipo($newName);
        if(!$_teamNew->existeEquipo()){
            $args = array('nombre'=>$newName);
            $_team->updateTeam($args);

            $name = $newName;
            $message = 'Usuario Modificado '.$name;

        }else{
            $status = 'error';
            $message = 'ya existe '.$newName;
        }
    }

}else{
    $status = 'error';
    $message = 'No existe '.$name;
}



$team = array();
$team['title']=$name;
$team['players']=$players;
$team['total']=count($players);


$response = array();
$response['action']=$action;
$response['status']=$status;
$response['message']=$message;
$response['result']=$team;

echo json_encode($response,JSON_FORCE_OBJECT);