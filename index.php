<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>TEST</title>

    <style>
        .viewOption{display:none;}
        .viewOption.show{display:block;}
        .viewOption .update{display: none;}
        .viewOption .update.show{display: block;}

        .table{
            max-width: 400px;
            border: 1px solid #666;
            border-radius: 6px;
            box-shadow: 2px 2px 5px 0px rgba(50, 50, 50, 0.75);
        }
        .table .title{
            padding:10px;
            color: #aaa;
            font-size: 20px;
            font-weight: bold;
            text-align: center;
            text-shadow: 0px 0px 4px #000;
            background: #aaa;
        }
        .table .content{}
        .table .content .item{display: flex; flex-flow: wrap;}
        .table .content .item .cell{ width: 33%; }

    </style>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).on('change','select[name="action"]',function(){
            var value = $(this).val();
            if(value != ''){
                $('.viewOption').addClass('show');

            }else{
                $('.viewOption').removeClass('show');
            }

            if(value == 'updateTeam'){
                $('.viewOption .update').addClass('show');

            }else{
                $('.viewOption .update').removeClass('show');
            }
        });


        function formSubmit(form){
            var form = $(form);
            var action = form.find('select[name="action"]').val();
            var team = form.find('input[name="team"]').val();
            var newName = form.find('input[name="newName"]').val();

            if(action == ''){
                alert("debe seleccionar una accion");
            }

            if(action != '' && team == ''){
                alert("debe escribir un equipo");
                return false;
            }

            if(action == 'updateTeam' && (team == '' || newName == '')){
                alert("Debe llenar los campos");
                return false;
            }


            $.ajax({
                method: 'POST',
                url: 'api.php',
                data: form.serialize(),
                success: function(data) {
                    console.log (data)
                    data = JSON.parse(data);
                    if(data.status == 'error'){
                        alert(data['message']);

                    }else if(data['status'] == 'success'){

                        if(data['action'] == 'viewTeam') {

                        }else if(data['action'] == 'updateTeam'){
                            alert(data['message']);
                        }

                        var result = data['result'];
                        var html = '<div class="title">'+result['title']+'</div>';

                        html += '<div class="content">';
                        for(player in result['players']){
                            html += '<div class="item">';
                            html += '<div class="cell">'+result['players'][player]['id']+'</div>';
                            html += '<div class="cell">'+result['players'][player]['nick']+'</div>';
                            html += '<div class="cell">'+result['players'][player]['summoner']+'</div>';
                            html += '</div>';
                        }
                        html += '</div>';
                        $('.table').html(html);
                    }


                }, error: function (resultat, statut, erreur) {
                    console.log(resultat);
                    console.log(statut);
                    console.log(erreur);
                }
            });

        }

    </script>
</head>

<body>
    <form onsubmit="formSubmit(this); return false;">
        <label>Accion</label>
        <select name="action">
            <option value="">-- Acción --</option>
            <option value="viewTeam">Ver equipo</option>
            <option value="updateTeam">Actualizar equipo</option>
        </select>

        <div class="viewOption">
            <div class="insert">
                <label>Equipo</label>
                <input type="text" name="team" value="", placeholder="Equipo"/>
            </div>

            <div class="update">
                <label>Nuevo nombre</label>
                <input type="text" name="newName" value="" placeholder="Nuevo nombre"/>
            </div>
        </div>

        <button type="submit">
            Enviar
        </button>
    </form>

<div class="table">
</div>

</body>
</html>


