<?php

class Connection{
	var $args;

	function __construct($args=array()){
		
		$this->args = $args;
		$this->hostname = "localhost";
		$this->database = "gaming";
		$this->username = "root";
		$this->password = "";
	}  
	
	function Connection($query='',$action=''){
		if($query!=''){
			$connection = mysqli_connect($this->hostname, $this->username, $this->password, $this->database);
			if (mysqli_connect_errno()) {
				printf("Falló la conexión: %s\n", mysqli_connect_error());
				exit();
			}
			
			//UTF-8 Encoding
			//$connection->character_set_name();
			//$connection->set_charset("utf8");
			mysqli_query($connection, "SET NAMES 'utf8'"); 
			mysqli_query($connection, "SET CHARACTER SET 'utf8'"); 
			
			$result = mysqli_query($connection, $query);
			if (!$result) {
				echo 'MySQL Error: '.mysqli_error($connection);
				exit;
			}
		
			if($action=='return'){
				return array('result'=>$result,'connection'=>$connection);
				
			}else if(!is_bool($result)){
				mysqli_free_result($result);
				mysqli_close($connection);
			}
		}
	}
	
	function General($query=''){
		$return=array(); 
		
		if($query!=''){
			$connect = $this->Connection($query,'return'); 
			$connection = $connect['connection'];
			$result = $connect['result'];
			
			$row = mysqli_fetch_assoc($result);  
			$count = mysqli_num_rows($result);
			if($count>0){
				do {
					$return[]=$row;
				}while($row = mysqli_fetch_assoc($result));
			}
			
			if(!is_bool($result)){
				mysqli_free_result($result);
				mysqli_close($connection);
			}
		}
		return $return;
	}
   
	function Fields($args=array()){
		$args_field=array();
		$args_value=array();
		$args_both=array();
		foreach($args as $field=>$value){
			if($value!=''){
				$args_field[]=$field;
				$args_value[]="'$value'";
				$args_both[]="$field='$value'";
			}
		}
		
		$fields=array();
		$fields['field']=$args_field;
		$fields['value']=$args_value;
		$fields['both']=$args_both;		
		return $fields;	
	} 
   
	function Insert($tabla='',$args=array()){
		if($tabla!='' && count($args)>0){
			$fields=$this->Fields($args);
			$group_field=implode(',',$fields['field']);
			$group_value=implode(',',$fields['value']);
			$insert = "INSERT INTO $tabla ($group_field) VALUES ($group_value);";
			$this->Connection($insert);		
		}
	} 
   
	function Update($tabla='',$args=array(),$where=''){
		if($tabla!='' && count($args)>0){
			$fields=$this->Fields($args);
			$group_both=implode(',',$fields['both']);
			$update = "UPDATE $tabla SET $group_both WHERE $where;";
			$this->Connection($update);		
		}
	} 
   
	function Delete($tabla='',$where=''){
		if($tabla!='' && $where!=''){
			$delete = "DELETE FROM $tabla WHERE $where;";
			$this->Connection($delete);		
		}
	} 
}
?>