<?php

class Equipo{
    private $id = 0;
    public $nombre = null;
    private $jugadores = array();
    private $message = "";
    private $conection;

    function __construct($value=""){
        global $_conection;
        $this->conection = $_conection;

        $where = " equipos.nombre = '$value'";
        if(is_numeric ($value)){
            $value = (int) $value;
            if(is_integer($value)){
                $where = " equipos.id = '$value'";
            }
        }

        $query = "SELECT * FROM equipos WHERE $where ORDER BY id DESC";
        $result = $this->conection->General($query);

        if(count($result)>0){
            $this->id = $result[0]['id'];
            $this->nombre = $result[0]['nombre'];

        }else{
            $this->id = 0;
            $this->nombre = null;
        }

        //$this->message = $where;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function existeEquipo(){
        if($this->nombre == null){
            return false;

        }else{
            return true;
        }
    }

    public function getJugadores(){
        if(!$this->existeEquipo()){
            return array();

        }else if(count($this->jugadores)>0){
            return $this->jugadores;

        }else{
            $team = $this->id;
            $query = "
                SELECT usuarios.id, usuarios.nick, summoners.summoner 
                FROM equipos_usuarios  
                LEFT JOIN summoners ON summoners.idusuario = equipos_usuarios.idusuario
                LEFT JOIN usuarios ON usuarios.id = equipos_usuarios.idusuario
                WHERE equipos_usuarios.idequipo = '$team' 
                ORDER BY id DESC";

            $this->jugadores =$this->conection->General($query);
            return $this->jugadores;
        }
    }

    function updateTeam($args){
        $id = $this->id;
        $this->conection->Update('equipos',$args,"id = '$id'");
        return;
    }

    public function message(){
        return $this->message;
    }
}